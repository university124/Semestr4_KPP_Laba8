package semy.laba_8.client;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import semy.laba_8.shared.Conferee;
import semy.laba_8.shared.RegistrationService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientController {

    @FXML
    private TextField hostField;
    @FXML
    private TextField portField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField familyField;
    @FXML
    private TextField organizationField;
    @FXML
    private TextField reportField;
    @FXML
    private TextField emailField;
    @FXML
    private TextArea infoArea;

    private RegistrationService service;

    @FXML
    public void initialize() {
        // Initial setup if needed
    }

    @FXML
    protected void onRegisterButtonClick() {
        try {
            String host = hostField.getText();
            int port = Integer.parseInt(portField.getText());
            Registry registry = LocateRegistry.getRegistry(host, port);
            service = (RegistrationService) registry.lookup("RegistrationService");

            String name = nameField.getText();
            String family = familyField.getText();
            String organization = organizationField.getText();
            String report = reportField.getText();
            String email = emailField.getText();

            Conferee conferee = new Conferee(name, family, organization, report, email);
            int count = service.registerConferee(conferee);

            infoArea.appendText("Registered: " + conferee + "\nTotal participants: " + count + "\n");
        } catch (Exception e) {
            infoArea.appendText("Error: " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }

    @FXML
    protected void onClearButtonClick() {
        nameField.clear();
        familyField.clear();
        organizationField.clear();
        reportField.clear();
        emailField.clear();
    }

    @FXML
    protected void onGetInfoButtonClick() {
        try {
            String host = hostField.getText();
            int port = Integer.parseInt(portField.getText());
            Registry registry = LocateRegistry.getRegistry(host, port);
            service = (RegistrationService) registry.lookup("RegistrationService");

            infoArea.clear();
            service.getAllConferees().forEach(conferee -> infoArea.appendText(conferee + "\n"));
        } catch (Exception e) {
            infoArea.appendText("Error: " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }

    @FXML
    protected void onFinishButtonClick() {
        System.exit(0);
    }
}
