package semy.laba_8.server;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import semy.laba_8.shared.Conferee;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class ServerController {

    @FXML
    private TextField hostField;
    @FXML
    private TextField portField;
    @FXML
    private TextField participantsField;
    @FXML
    private TextArea infoArea;

    private List<Conferee> confereeList = new ArrayList<>();

    @FXML
    public void initialize() {
        participantsField.setText("0");
    }

    @FXML
    protected void onStartButtonClick() {
        try {
            String host = hostField.getText();
            int port = Integer.parseInt(portField.getText());

            RegistrationServiceImpl service = new RegistrationServiceImpl();
            service.setServerController(this);  // передаем контроллер сервера в реализацию сервиса

            Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("RegistrationService", service);

            infoArea.appendText("Server started on host: " + host + " and port: " + port + "\n");
        } catch (Exception e) {
            infoArea.appendText("Error: " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }

    @FXML
    protected void onStopButtonClick() {
        System.exit(0);
    }

    @FXML
    protected void onSaveButtonClick() {
        // Implement saving to XML
    }

    @FXML
    protected void onLoadButtonClick() {
        // Implement loading from XML
    }

    @FXML
    protected void onExitButtonClick() {
        System.exit(0);
    }

    public void updateInfoArea(Conferee conferee) {
        confereeList.add(conferee);
        participantsField.setText(String.valueOf(confereeList.size()));

        StringBuilder info = new StringBuilder();
        for (Conferee c : confereeList) {
            info.append("New participant registered:\n")
                    .append(c).append("\n\n");
        }

        infoArea.setText(info.toString());
    }
}
