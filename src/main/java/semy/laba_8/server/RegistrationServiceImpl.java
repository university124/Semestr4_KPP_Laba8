package semy.laba_8.server;

import semy.laba_8.shared.Conferee;
import semy.laba_8.shared.RegistrationService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class RegistrationServiceImpl extends UnicastRemoteObject implements RegistrationService {

    private List<Conferee> confereeList = new ArrayList<>();
    private ServerController serverController;

    protected RegistrationServiceImpl() throws RemoteException {
        super();
    }

    @Override
    public synchronized int registerConferee(Conferee conferee) throws RemoteException {
        confereeList.add(conferee);
        serverController.updateInfoArea(conferee);
        return confereeList.size();
    }

    @Override
    public List<Conferee> getAllConferees() throws RemoteException {
        return new ArrayList<>(confereeList);
    }

    public void setServerController(ServerController serverController) {
        this.serverController = serverController;
    }
}
