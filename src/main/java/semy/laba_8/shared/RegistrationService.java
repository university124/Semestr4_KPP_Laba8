package semy.laba_8.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RegistrationService extends Remote {
    int registerConferee(Conferee conferee) throws RemoteException;
    List<Conferee> getAllConferees() throws RemoteException;
}
