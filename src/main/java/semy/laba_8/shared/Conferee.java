package semy.laba_8.shared;

import java.io.Serializable;

public class Conferee implements Serializable {
    private String name;
    private String familyName;
    private String organization;
    private String reportTitle;
    private String email;

    public Conferee(String name, String familyName, String organization, String reportTitle, String email) {
        this.name = name;
        this.familyName = familyName;
        this.organization = organization;
        this.reportTitle = reportTitle;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getOrganization() {
        return organization;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Name: " + name + "\n" +
                "Family Name: " + familyName + "\n" +
                "Organization: " + organization + "\n" +
                "Report Title: " + reportTitle + "\n" +
                "Email: " + email;
    }
}
