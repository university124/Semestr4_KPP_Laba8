module semy.laba_8 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.rmi;

    opens semy.laba_8.client to javafx.fxml;
    opens semy.laba_8.server to javafx.fxml;
    opens semy.laba_8.shared to javafx.base;

    exports semy.laba_8.client;
    exports semy.laba_8.server;
    exports semy.laba_8.shared;
}
